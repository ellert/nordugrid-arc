Source: nordugrid-arc
Section: net
Priority: optional
Maintainer: Mattias Ellert <mattias.ellert@physics.uu.se>
Uploaders: Anders Waananen <waananen@nbi.dk>
Build-Depends:
 debhelper-compat (= 13),
 dpkg-dev (>= 1.22.5),
 dh-python,
 libxml2-dev (>= 2.4.0),
 libssl-dev,
 libglibmm-2.4-dev,
 libltdl-dev,
 libldap2-dev,
 uuid-dev,
 libcppunit-dev,
 pkgconf,
 libdb5.3++-dev,
 libxmlsec1-dev (>= 1.2.4),
 libglobus-common-dev,
 libglobus-gssapi-gsi-dev,
 libglobus-ftp-client-dev,
 libglobus-ftp-control-dev,
 libxrootd-client-dev,
 libgfal2-dev,
 openssl,
 swig,
 libnss3-dev,
 libjson-xs-perl,
 libxml-simple-perl,
 libdbi-perl,
 libsqlite3-dev (>= 3.6),
 libldns-dev,
 libsystemd-dev [linux-any],
 libinline-python-perl (>= 0.56-2~),
 bash-completion,
 python3-dev,
 python3-setuptools
Standards-Version: 4.7.1
Vcs-Browser: https://salsa.debian.org/ellert/nordugrid-arc
Vcs-Git: https://salsa.debian.org/ellert/nordugrid-arc.git
Homepage: http://www.nordugrid.org

Package: libarccommon3t64
Provides: ${t64:Provides}
X-Time64-Compat: libarccommon3v5
Replaces:
 nordugrid-arc-hed (<< 1.0.1~rc2~),
 nordugrid-arc-arex (<< 2.0.1~),
 libarccommon0,
 libarccommon1,
 libarccommon2,
 libarccommon3,
 libarccommon3v5
Conflicts:
 nordugrid-arc-chelonia (<< 2.0.0~),
 nordugrid-arc-hopi (<< 2.0.0~),
 nordugrid-arc-isis (<< 2.0.0~),
 nordugrid-arc-janitor (<< 2.0.0~),
 nordugrid-arc-doxygen (<< 4.0.0~),
 nordugrid-arc-arcproxyalt (<< 6.0.0~),
 nordugrid-arc-java (<< 6.0.0~),
 nordugrid-arc-egiis (<< 6.0.0~),
 nordugrid-arc-python (<< 6.0.0~),
 nordugrid-arc-acix-cache (<< 6.0.0~),
 nordugrid-arc-acix-core (<< 6.14.0-2~),
 nordugrid-arc-acix-scanner (<< 6.14.0-2~),
 nordugrid-arc-acix-index (<< 6.14.0-2~)
Breaks:
 nordugrid-arc-hed (<< 1.0.1~rc2~),
 nordugrid-arc-arex (<< 2.0.1~),
 libarccommon0,
 libarccommon1,
 libarccommon2,
 libarccommon3,
 libarccommon3v5 (<< ${source:Version})
Architecture: any
Section: libs
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 ${python3:Depends},
 openssl
Description: Advanced Resource Connector Middleware
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 The ARC middleware is a software solution that uses distributed
 computing technologies to enable sharing and federation of computing
 resources across different administrative and application domains.
 ARC is used to create distributed infrastructures of various scope and
 complexity, from campus to national and global deployments.

Package: nordugrid-arc-client
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libarccommon3t64 (= ${binary:Version}),
 nordugrid-arc-plugins-needed (= ${binary:Version})
Description: ARC command line clients
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 This client package contains all the CLI tools that are needed to
 operate with x509 proxies, submit and manage jobs and handle data
 transfers.

Package: nordugrid-arc-hed
Replaces:
 nordugrid-arc-client (<< 1.0.1~rc2~)
Breaks:
 nordugrid-arc-client (<< 1.0.1~rc2~)
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libarccommon3t64 (= ${binary:Version})
Description: ARC Hosting Environment Daemon
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 The ARC Hosting Environment Daemon (HED) is a Web Service container
 for ARC services.

Package: nordugrid-arc-gridftpd
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libarccommon3t64 (= ${binary:Version}),
 nordugrid-arc-plugins-gridftp (= ${binary:Version}),
 nordugrid-arc-arcctl-service (= ${source:Version})
Description: ARC GridFTP server
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 This package contains the ARC gridftp server which can be used as a
 custom job submission interface in front of an ARC enabled computing
 cluster or as a low-level dedicated gridftp file server.

Package: nordugrid-arc-datadelivery-service
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libarccommon3t64 (= ${binary:Version}),
 nordugrid-arc-hed (= ${binary:Version}),
 nordugrid-arc-plugins-needed (= ${binary:Version}),
 nordugrid-arc-arcctl-service (= ${source:Version})
Description: ARC data delivery service
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 This package contains the ARC data delivery service.

Package: nordugrid-arc-infosys-ldap
Provides:
 nordugrid-arc-ldap-infosys,
 nordugrid-arc-aris
Replaces:
 nordugrid-arc-ldap-infosys (<< 6.0.0~),
 nordugrid-arc-aris (<< 6.0.0~),
 nordugrid-arc-ldap-egiis (<< 6.0.0~)
Conflicts:
 nordugrid-arc-ldap-infosys (<< 6.0.0~),
 nordugrid-arc-aris (<< 6.0.0~),
 nordugrid-arc-ldap-egiis (<< 6.0.0~)
Architecture: all
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${misc:Depends},
 ${perl:Depends},
 slapd,
 glue-schema (>= 2.0.10),
 bdii,
 nordugrid-arc-arcctl-service (= ${source:Version})
Description: ARC LDAP-based information services
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 This package contains the ARC information services relying on BDII and
 LDAP technologies to publish ARC CE information according to various
 LDAP schemas. Please note that the information collectors are part of
 another package, the nordugrid-arc-arex.

Package: nordugrid-arc-monitor
Replaces:
 nordugrid-arc-ldap-monitor (<< 6.0.0~),
 nordugrid-arc-ws-monitor (<< 6.0.0~)
Conflicts:
 nordugrid-arc-ldap-monitor (<< 6.0.0~),
 nordugrid-arc-ws-monitor (<< 6.0.0~)
Architecture: all
Depends:
 ${misc:Depends},
 php-common,
 php-ldap,
 php-gd
Description: ARC LDAP monitor web application
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 This package contains the PHP web application that is used to set up a
 web-based monitor which pulls information from the LDAP information
 system and visualizes it.

Package: nordugrid-arc-arcctl
Replaces:
 libarccommon3v5 (<< 6.5.0~),
 nordugrid-arc-arex (<< 6.5.0~)
Breaks:
 libarccommon3v5 (<< 6.5.0~),
 nordugrid-arc-arex (<< 6.5.0~)
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
 libarccommon3t64 (>= ${source:Version})
Description: ARC Control Tool
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 This package contains the ARC Control Tool with basic set of control
 modules suitable for both server and client side.

Package: nordugrid-arc-arcctl-service
Replaces:
 libarccommon3v5 (<< 6.5.0~),
 nordugrid-arc-arcctl (<< 6.6.0~),
 nordugrid-arc-arex (<< 6.6.0~)
Breaks:
 libarccommon3v5 (<< 6.5.0~),
 nordugrid-arc-arcctl (<< 6.6.0~),
 nordugrid-arc-arex (<< 6.6.0~)
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
 libarccommon3t64 (>= ${source:Version}),
 nordugrid-arc-arcctl (= ${source:Version})
Description: ARC Control Tool - service control modules
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 This package contains the service control modules for ARC Contol Tool
 that allow working with server-side config and manage ARC services.

Package: nordugrid-arc-arex
Provides:
 nordugrid-arc-cache-service,
 nordugrid-arc-candypond
Replaces:
 nordugrid-arc-cache-service (<< 6.0.0~),
 nordugrid-arc-candypond (<< 6.0.0~),
 nordugrid-arc-aris (<< 6.0.0~),
 libarccommon3v5 (<< 6.5.0~)
Conflicts:
 nordugrid-arc-cache-service (<< 6.0.0~),
 nordugrid-arc-candypond (<< 6.0.0~)
Breaks:
 nordugrid-arc-aris (<< 6.0.0~),
 libarccommon3v5 (<< 6.5.0~)
Architecture: any
Pre-Depends:
 ${misc:Pre-Depends}
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 ${perl:Depends},
 ${python3:Depends},
 libarccommon3t64 (= ${binary:Version}),
 nordugrid-arc-hed (= ${binary:Version}),
 nordugrid-arc-plugins-needed (= ${binary:Version}),
 nordugrid-arc-arcctl-service (= ${source:Version}),
 libjson-xs-perl,
 libxml-simple-perl,
 procps
Description: ARC Resource-coupled EXecution service
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 The ARC Resource-coupled EXecution service (AREX) is the Computing
 Element of the ARC middleware. AREX offers a full-featured middle
 layer to manage computational tasks including interfacing to local
 batch systems, taking care of complex environments such as data
 staging, data caching, software environment provisioning, information
 collection and exposure, accounting information gathering and
 publishing.

Package: nordugrid-arc-arex-python-lrms
Replaces:
 nordugrid-arc-arex (<< 6.0.0~)
Breaks:
 nordugrid-arc-arex (<< 6.0.0~)
Architecture: any
Depends:
 ${misc:Depends},
 ${python3:Depends},
 libinline-python-perl (>= 0.56-2~),
 nordugrid-arc-arex (= ${binary:Version}),
 python3-nordugrid-arc (= ${binary:Version})
Description: ARC Resource-coupled EXecution service - Python LRMS backends
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 The Python LRMS backends are a new implementation of the AREX LRMS
 backend scripts written in Python. Currently only the SLURM LRMS is
 supported. It is released as a technology preview.

Package: nordugrid-arc-community-rtes
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
 nordugrid-arc-arex (>= ${source:Version}),
 nordugrid-arc-arcctl (= ${source:Version}),
 python3-dnspython
Description: ARC community defined RTEs support
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 Community RTEs is the framework that allows deploying software packages
 (tarballs, containers, etc) provided by trusted communities to ARC CE
 using simple arcctl commands.
 It is released as a technology preview.

Package: nordugrid-arc-plugins-needed
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libarccommon3t64 (= ${binary:Version})
Description: ARC base plugins
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 ARC base plugins. This includes the Message Chain Components (MCCs)
 and Data Manager Components (DMCs).

Package: libarcglobusutils3t64
Provides: ${t64:Provides}
Replaces:
 libarcglobusutils3,
 nordugrid-arc-plugins-globus (<< 6.5.0~)
Breaks:
 libarcglobusutils3 (<< ${source:Version}),
 nordugrid-arc-plugins-globus (<< 6.5.0~)
Architecture: any
Section: libs
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libarccommon3t64 (= ${binary:Version})
Description: ARC Globus plugins common libraries
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 ARC Globus plugins common libraries package includes the bundle of
 necessary Globus libraries needed for all other globus-dependent ARC
 components.

Package: nordugrid-arc-plugins-gridftp
Replaces:
 nordugrid-arc-plugins-globus (<< 6.5.0~)
Breaks:
 nordugrid-arc-plugins-globus (<< 6.5.0~)
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libarccommon3t64 (= ${binary:Version}),
 libarcglobusutils3t64 (= ${binary:Version})
Description: ARC Globus dependent DMCs
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 ARC Globus GridFTP plugins. These allow access to data through the
 gridftp protocol.

Package: nordugrid-arc-plugins-lcas-lcmaps
Replaces:
 nordugrid-arc-plugins-globus (<< 6.5.0~)
Breaks:
 nordugrid-arc-plugins-globus (<< 6.5.0~)
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libarccommon3t64 (= ${binary:Version}),
 libarcglobusutils3t64 (= ${binary:Version})
Description: ARC LCAS/LCMAPS plugins
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 ARC LCAS/LCMAPS tools allow configuring ARC CE to use LCAS/LCMAPS
 services for authorization and mapping.

Package: nordugrid-arc-plugins-gridftpjob
Replaces:
 nordugrid-arc-plugins-globus (<< 6.5.0~)
Breaks:
 nordugrid-arc-plugins-globus (<< 6.5.0~)
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libarccommon3t64 (= ${binary:Version}),
 libarcglobusutils3t64 (= ${binary:Version}),
 nordugrid-arc-plugins-gridftp (= ${binary:Version})
Description: ARC GRIDFTPJOB client plugin
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 ARC GRIDFTPJOB plugin allows submitting jobs via the gridftpd interface.

Package: nordugrid-arc-plugins-xrootd
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libarccommon3t64 (= ${binary:Version})
Description: ARC xrootd plugins
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 ARC xrootd plugins. These allow access to data through the xrootd
 protocol.

Package: nordugrid-arc-plugins-gfal
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libarccommon3t64 (= ${binary:Version})
Description: ARC GFAL2 plugins
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 ARC plugins for GFAL2. This allows third-party transfer and adds
 support for several extra transfer protocols (rfio, dcap, gsidcap).
 Support for specific protocols is provided by separate 3rd-party GFAL2
 plugin packages.

Package: nordugrid-arc-plugins-internal
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libarccommon3t64 (= ${binary:Version}),
 nordugrid-arc-arex (= ${binary:Version})
Description: ARC internal plugin
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 The ARC internal plugin. A special interface aimed for restrictive HPC
 sites, to be used with a local installation of the ARC Control Tower.

Package: nordugrid-arc-plugins-arcrest
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libarccommon3t64 (= ${binary:Version})
Description: ARC REST plugin
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 ARC plugin for ARC REST interface technology preview.

Package: nordugrid-arc-plugins-python
Replaces:
 python3-nordugrid-arc (<< 6.6.0~)
Breaks:
 python3-nordugrid-arc (<< 6.6.0~)
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libarccommon3t64 (= ${binary:Version}),
 python3-nordugrid-arc (= ${binary:Version})
Description: ARC Python dependent plugin
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 ARC plugins dependent on Python.

Package: nordugrid-arc-dev
Replaces:
 python3-nordugrid-arc (<< 6.6.0~)
Breaks:
 python3-nordugrid-arc (<< 6.6.0~)
Architecture: any
Section: libdevel
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libarccommon3t64 (= ${binary:Version}),
 libxml2-dev (>= 2.4.0),
 libssl-dev,
 libglibmm-2.4-dev
Description: ARC development files
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 Header files and libraries needed to develop applications using ARC.

Package: python3-nordugrid-arc
Provides:
 ${python3:Provides}
Architecture: any
Section: python
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 ${python3:Depends},
 libarccommon3t64 (= ${binary:Version})
Description: ARC Python wrapper
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 Python bindings for ARC.

Package: nordugrid-arc-nordugridmap
Provides:
 nordugrid-arc-gridmap-utils
Replaces:
 nordugrid-arc-gridmap-utils (<< 6.0.0~)
Conflicts:
 nordugrid-arc-gridmap-utils (<< 6.0.0~)
Architecture: all
Depends:
 liblwp-protocol-https-perl,
 libsoap-lite-perl,
 liburi-perl,
 libxml-dom-perl,
 ${perl:Depends},
 ${misc:Depends}
Recommends:
 cron
Description: ARC's nordugridmap tool
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 A simple tool to fetch list of users and eventually generate gridmap
 files.

Package: nordugrid-arc-test-utils
Provides:
 nordugrid-arc-misc-utils
Replaces:
 nordugrid-arc-misc-utils (<< 6.0.0~)
Conflicts:
 nordugrid-arc-misc-utils (<< 6.0.0~)
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 libarccommon3t64 (= ${binary:Version}),
 nordugrid-arc-plugins-needed (= ${binary:Version})
Description: ARC test tools
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 This package contains a few utilities useful to test various ARC
 subsystems. The package is not required by users or sysadmins and it
 is mainly for developers.

Package: nordugrid-arc-archery-manage
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
 python3-ldap,
 python3-dnspython
Description: ARCHERY administration tool
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 This package contains the archery-manage utility for administration of
 an ARCHERY DNS-embedded service endpoint registry.

Package: nordugrid-arc-wn
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends}
Description: ARC optional worker nodes components
 NorduGrid is a collaboration aiming at development, maintenance and
 support of the middleware, known as the Advanced Resource
 Connector (ARC).
 .
 This package contains the optional components that provide new job
 management features on the worker nodes (WN).
